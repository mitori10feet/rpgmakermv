//=============================================================================
// Chimaki_ErrorLogjs
// Version: 1.0
//=============================================================================
/*:
 * @plugindesc 紀錄錯誤log, 方便其他人協助查詢問題
 * @author Chimaki
 *
 * @help 作者網站 Maker 製造機 http://www.chimakier.com
 * 此插件純粹為協助除錯功能，當你發布遊戲給朋友玩遇到程式上的錯誤時，會自動將錯誤log儲存到 save/log.txt這隻檔案裡面
 * 
 */

'use strict'; // es mode


(function(){


	StorageManager.writelog = function (str){
		
		var fs = require('fs');
		var path = require('path');
	    var base = path.dirname(process.mainModule.filename);

	    fs.appendFile(base + '/save/log.txt' , str + "\n", function(err){
	     	if (err) {
	          	console.error(err);
	    	 }
		});
	}

	SceneManager.catchException = function(e) {
		StorageManager.writelog('Error , ' + e.name + ',file name ,' + e.message + ', line ,' + e.stack);
	    if (e instanceof Error) {
	        Graphics.printError(e.name, e.message);
	        console.error(e.stack);
	    } else {
	        Graphics.printError('UnknownError', e);
	    }
	    AudioManager.stopAll();
	    this.stop();
	};

	SceneManager.onError = function(e) {
	    console.error(e.message);
	    console.error(e.filename, e.lineno);
	    StorageManager.writelog('Error , ' + e.message + ',file name ,' + e.filename + ', line ,' + e.lineno);
	    try {
	        this.stop();
	        Graphics.printError('Error', e.message);
	        AudioManager.stopAll();
	    } catch (e2) {
	    	
	    }
	};



})();