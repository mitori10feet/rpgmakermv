//=============================================================================
// Chimaki_TakeScreenShot.js
// Version: 1.0
// http://www.chimakier.com
//=============================================================================
/*
 * @plugindesc SLG相關內容
 * @author Chimaki
 *
 * @param 
 * @desc 
 * @default 0
 *
 * @help 作者網站 Maker 製造機 http://www.chimakier.com
 * 
 *  里德戰鬥畫面 沒有閃光
 */

'use strict';


var scene_key_down = SceneManager.onKeyDown;
SceneManager.onKeyDown = function(event) {
    scene_key_down.call(this, event);
    if (!event.ctrlKey && !event.altKey) {
        switch (event.keyCode) {
            case 80:
                playerSnapShop();
                break;            
        }
    }
    
};

SceneManager.clear_shot = function (){
    this._temp_shop = null;
}

SceneManager.get_game_shot = function() {
    if (!this._temp_shop) this.set_game_shot();
    return this._temp_shop;
    
};

SceneManager.set_game_shot = function (){
    this._temp_shop = Bitmap.snap(this._scene);
}



function playerSnapShop () {

		SceneManager.set_game_shot();


		let now =  Date.now();
		let fs = require('fs');
	    let path = require('path');
	    let base = path.dirname(process.mainModule.filename);
	    path = path.join(base, 'screenshot/');

		let bitmap = SceneManager.get_game_shot();;
		
		let urlData = bitmap.canvas.toDataURL('png',100);		
		let base64Data = urlData.replace( (/^data:image\/png;base64,/) , "");
		let last_path = path ;
	    if (!fs.existsSync(last_path)) {
	        fs.mkdirSync(last_path);
	    }

		let str =  last_path + now ;
		fs.writeFileSync(str + ".png", base64Data, 'base64');
		SceneManager.clear_shot();	

		// alert("save screenshot to " + str );
}
