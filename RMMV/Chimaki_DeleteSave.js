//=============================================================================
// Chimaki_DeleteSave.js
// Version: 1.0
//=============================================================================
/*:
* @plugindesc 刪除save檔案
* @author Chimaki 
* 
* ============================================================================
* @help
* 此插件由 Maker 製造機粽子 撰寫，禁止二次發佈
* 使用此插件在遊戲中時，希望可以附上來源網址
* 來源網址 : http://www.chimakier.com
* 
* 插件指令 輸入 SAVEDELETE 就可以刪除所有save檔案
* 插件指令 輸入 SAVEDELETE NOW 刪除這一次讀取資料時的檔案
*/
//=============================================================================
'use strict'; // es mode

var Imported = Imported || {};
Imported._alias = {};
Imported._args = {};

(function (){

    Imported._alias._plugin_command = Game_Interpreter.prototype.pluginCommand;
    Game_Interpreter.prototype.pluginCommand = function(command, args) {
        Imported._alias._plugin_command.call(this, command, args);
    	if (command == "SAVEDELETE"){
    		if (args[0] == "NOW"){
    			StorageManager.removeAll(Number(Imported._args._load_id));
    		}
    		else {
    			StorageManager.removeAll();
    		}

    	}
    }
    Imported._alias._scene_loasuccess = Scene_Load.prototype.onLoadSuccess;
	Scene_Load.prototype.onLoadSuccess = function() {
		Imported._alias._scene_loasuccess.call(this);
		Imported._args._load_id = this.savefileId();
	};
	Imported._alias._scene_svesuccess = Scene_Load.prototype.onSavefileOk;
	Scene_Load.prototype.onSavefileOk = function() {
		Imported._alias._scene_svesuccess.call(this);
		Imported._args._load_id = this.savefileId();

	};	

})();
StorageManager.removeAll = function(id) {
	if (!id){
		for (var i = 1 ; i <= 30; i++){
			this.remove(i);	
		}		
	}
	else {
		this.remove(id);	
	}
};



