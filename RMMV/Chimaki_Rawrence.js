//=============================================================================
// Chimaki_Rawrence.js
// Version: 0.0.1
//=============================================================================
/*:
* @plugindesc 各種小工具
* @author Chimaki 
* 
* @param status_x
* @desc 狀態頁面數值座標X 微調
* @default 0
*
* @param status_w
* @desc 狀態頁面數值最大寬度增加
* @default 100
*
* ============================================================================
* 作者: 粽子(chimakki) 
* 作者網站 Marker 製造機:www.chimakier.com
* 
* 此插件粽子替作者解決問題時 替作者寫的小插件
*/

//=============================================================================
// 
//=============================================================================
var Imported = Imported || {};
Imported.Chimaki_Tool = {};

(function(){
	var ChiamkiTool_parameters = PluginManager.parameters('Chimaki_Rawrence');
	var Chimaki_Tool = Imported.Chimaki_Tool;

	Chimaki_Tool.offsetX = Math.floor(ChiamkiTool_parameters['status_x']|| 0);
	Chimaki_Tool.offsetW = Math.floor(ChiamkiTool_parameters['status_w']|| 100);


//=============================================================================
// 修改參數x座標跟最大寬度
//=============================================================================
	Window_Status.prototype.drawParameters = function(x, y) {
	    var lineHeight = this.lineHeight();
	    for (var i = 0; i < 6; i++) {
	        var paramId = i + 2;
	        var y2 = y + lineHeight * i;
	        this.changeTextColor(this.systemColor());
	        this.drawText(TextManager.param(paramId), x, y2, 160);
	        this.resetTextColor();
	        this.drawText(this._actor.param(paramId), x + 160 + Chimaki_Tool.offsetX, y2, 60 + Chimaki_Tool.offsetW , 'right');
	    }
	};




}());