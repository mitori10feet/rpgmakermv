/*:
 * @plugindesc 圖片地圖設置
 * @author Chimaki
 *
 * 
 * 
 *   
 * @help
 * 此插件為Maker製造機所撰寫, 請勿二次發佈, 轉載請附上來源
 * Maker製造機網址 http://www.chimakier.com
 * 使用方法:
 * 在地圖的備註欄中輸入 
 * <底圖:圖片名稱> 蓋住地圖底圖的圖片圖層
 * <覆蓋:圖片名稱> 覆蓋玩家的圖片圖層
 *
 *
 * 動態立繪,一樣再地圖編輯器中設置, 可以達到轉場就先load 圖片的效果
 * <立繪1:圖片名稱;禎數>
 * 如果有多張圖, 請用數字往下疊加
 *
 * EX: 
 * <立繪1:圖片名稱;禎數>
 * <立繪2:圖片名稱;禎數> 
 *
 * 圖片名稱請依照聯播順序在檔案名稱加上 _001 _002 (3位數)
 * EX:
 * 我要在遊戲中顯示 連撥 AAA_01 ~ AAA_99, 該如何設置？
 * 
 * 地圖編輯器中設定, 這樣就會先設定好資料
 * <立繪1:AAA;5>
 * 
 * 接著, 你想讓AAA顯示的時候, 請在插件指令中輸入
 * CHARA 顯示 AAA x y 禎數 左右反轉
 *  x : 座標x
 *  y : 座標y
 *  禎數 : 數字, 更新頻率(可以不輸入)
 *  左右反轉 : 數字 0 => 不反轉 , 數字1 => 反轉
* 
 * 想要關閉則輸入
 * CHARA 關閉 AAA      
 *    
 *       
 * 圖片請置於 專案目錄/img/pictures/ 目錄下
 * 
 */
"use strict";
var chimaki_mapimg = chimaki_mapimg || {};
chimaki_mapimg.alias = {};
chimaki_mapimg.alias.command = Game_Interpreter.prototype.pluginCommand;
Game_Interpreter.prototype.pluginCommand = function(command, args) {
    chimaki_mapimg.alias.command.call(this, command, args);

    if (command == "CHARA"){
        if (args[0]){
            var scene = SceneManager._scene;
            switch (args[0]){
                case "顯示":
                    scene.showChara({
                        key : args[1],
                        x : Number(args[2]),                        
                        y : Number(args[3]),                        
                        frame : Number(args[4]), 
                        scale : Number(args[5]),
                        visible : true,
                    });
                    break;
                case "關閉":
                    scene.hideChara({
                        key : args[1]
                    });                
                    break;
            }
        }
    }
}
Scene_Map.prototype.showMapImage = function (key){
    let layerkey = this.mapImageKey(key);
    let layer = eval ( "this._spriteset._" + layerkey + "Layer");
    if (layer) {
        layer.visible = true;
    }
}
Scene_Map.prototype.hideChara = function (data){
    let layerList = this._spriteset.charaList();   
    let charLayer;
    layerList.forEach(function (sp){
        if (sp.charaImgKey() == data.key){
            charLayer = sp;
        }
    });  
    if (!charLayer){
        return;
    }

    charLayer.visible = false;  
}


Scene_Map.prototype.showChara = function (data){
    let layerList = this._spriteset.charaList();
    let charLayer;
    layerList.forEach(function (sp){
        if (sp.charaImgKey() == data.key){
            charLayer = sp;
        }
    });

    if (!charLayer){
        return;
    }
    charLayer.setCharStat(data);
}


Scene_Map.prototype.getSprite = function (key){
    let layerkey = this.mapImageKey(key)
    return eval ( "this._spriteset._" + layerkey + "Layer");
}




Scene_Map.prototype.hideMapImage = function (key){
    let layerkey = this.mapImageKey(key);
    let layer = eval ( "this._spriteset._" + layerkey + "Layer");
    if (layer) {
        layer.visible = false;
    }
}
Scene_Map.prototype.mapImageKey = function (key){
    let layerkey;
    switch (key){
        case "底圖":
            layerkey = 'below_tilemap';
            break;        
        case "覆蓋":
            layerkey = 'up_player'; 
        case "立繪":
            layerkey = 'charaImg';             
            break;
    }
    return layerkey;
}

Spriteset_Map.prototype.createLowerLayer = function() {
    Spriteset_Base.prototype.createLowerLayer.call(this)
    this.setUpDataMapInfo();
    this.createParallax();
    this.createTilemap();
    this.createBelowCharacters(this._mapImageInfo['below_tilemap']);
    this.createCharacters();
    this.createUpCharacters(this._mapImageInfo['up_player']);
    this.createShadow();
    this.createWeather();
    this.createCharaImage(this._mapImageInfo['charaImage']);
    this.createDestination();

};
Spriteset_Map.prototype.getLayer = function ( key) {
	return eval("SceneManager._scene._spriteset._" + key + "Layer")
}
Spriteset_Map.prototype.createPicturesLayer = function (key){

}
Spriteset_Map.prototype.mapImaginfo = function (){
    return this._mapImageInfo;
}
Spriteset_Map.prototype.setUpDataMapInfo = function (){
    this._mapImageInfo = {};
    let regStand = /立繪[ ]*(.*)/i;
    let meta = $dataMap.meta;


    if (meta['遠景下層']){
    };
    if (meta['底圖']){
        this._mapImageInfo['below_tilemap'] = meta['底圖'];
    };
    if (meta['覆蓋']){
        this._mapImageInfo['up_player'] = meta['覆蓋'];
    };
    if (meta['玩家上層']){
    };
    if (meta['天氣上層']){
    };
    for (let i in meta){
        let text = regStand.exec(i);
        if (i.match(/立繪\d/)){
            this._mapImageInfo['charaImg'] = this._mapImageInfo['charaImg'] || {}
            this._mapImageInfo['charaImg'][i+''] = meta[i];
        }
    }
}

Spriteset_Map.prototype.createBelowCharacters = function (name){
    if (!name ) return;
    this._below_tilemapLayer = new Sprite_MapImage(ImageManager.loadPicture(name))
    this._below_tilemapLayer.z = 0;
    this._tilemap.addChild( this._below_tilemapLayer);
}
Spriteset_Map.prototype.createUpCharacters = function (name){
    if (!name ) return;
    this._up_playerLayer = new Sprite_MapImage(ImageManager.loadPicture(name))
    this._up_playerLayer.z = 9;
    this._tilemap.addChild(this._up_playerLayer);
}
Spriteset_Map.prototype.charaList = function (){
    return this._charaImgArray;
}
Spriteset_Map.prototype.createCharaImage = function (){
    this._charaImgArray = this._charaImgArray || [];
    this._charaImgLayer = new Sprite();
    //<立繪:檔案名稱;更新頻率>


    let setting = this._mapImageInfo['charaImg']
    for (let i in setting ){
        // 看用幾張圖, 先塞到layer , 並且給予key, 讓command 可以控制
        let data = setting[i].split(';');

        let imagename = data[0];
        let frame = Number(data[1]);
        let sp  = new SpriteCharaImg();
        sp.visible = false;
        sp.setCharFrame(frame);
        sp._charaKey = imagename;


        this._charaImgLayer.addChild(sp);
        this._charaImgArray.push(sp);
        let id = 1; // for sub sp 
        let count = 1;
        let flag = 0;
        while (1){
            let img_name = imagename + '_' + count.padZero(3);
            let check = checkImage(img_name);
            if (!check) break;
            let child_sp = new Sprite(ImageManager.loadPicture(img_name));
            child_sp.visible = false;
            child_sp._id = id;
            sp.addChild(child_sp); // 塞入不同key
            flag = 1;
            id++;
            count++;
        }

        // 整理圖層, 好像也不用
        sp.children.sort(function(a, b){
            if (a._id < b._id) return -1;
            if (a._id > b._id) return 1;            
            return 0;
        })
        sp.setImgNum(flag ? count - 1: 0 );
    }
    this.addChild(this._charaImgLayer);
}
function checkImage (name){
    var fs = require('fs');
    var path = require('path');
    var base = path.dirname(process.mainModule.filename);       
    let str = base + '/img/pictures/' + name +'.png';
    var url = str;
    try{
        fs.statSync(url);
    } catch(e){
        return false;
    }

    return true;

}

function SpriteCharaImg (){
    this.initialize.apply(this, arguments);
}
SpriteCharaImg.prototype = Object.create(Sprite.prototype);
SpriteCharaImg.prototype.constructor = SpriteCharaImg;
SpriteCharaImg.prototype.initialize = function (bitmap){
    Sprite.prototype.initialize.call(this, bitmap);
    this._frameCount = 0;
    this._playNumber = 1;
    this._needLoop = true;
    this._startNum = 1;
    this._startFlag = 1;

}

SpriteCharaImg.prototype.charaImgKey = function (){
    return this._charaKey;
}
SpriteCharaImg.prototype.setCharStat = function (data){
    

    if (data.frame){
        this.setCharFrame(data.frame);
    }
    this.x = data.x ? data.x : this.x;
    this.y = data.y ? data.y : this.y;
    if (data.scale){
        this.scale.x *= -1;    
    }
    this._playNumber = this._startNum;
    this.visible = data.visible;
}

SpriteCharaImg.prototype.waitFrameCount = function (){
    if (this._startFlag){
        this._startFlag = 0;
        return true;
    }

    return this._frameCount > 0;
}
SpriteCharaImg.prototype.setCharFrame = function (frame){
    this._updateFrame = Number(frame) || 5;
}
SpriteCharaImg.prototype.setPlayNum = function (num){
    this._playNumber = num
}
SpriteCharaImg.prototype.playNum = function (){
    return this._playNumber;
}
SpriteCharaImg.prototype.setLoop = function (loop){
    this._needLoop = loop ;
}

SpriteCharaImg.prototype.setImgNum = function (num){
    this._updateNum = num ;        
}
SpriteCharaImg.prototype.resetFrameCount = function (){
    this._frameCount = this._updateFrame;
}
SpriteCharaImg.prototype.addPlayNum = function (){
    if (this._playNumber < this._updateNum){
        this._playNumber++;
    }
    else {
        this._playNumber = 1;
    }
}
SpriteCharaImg.prototype.update = function (){
    this.updateSubImag();
}
SpriteCharaImg.prototype.updateFrameCount = function (){
    if (this._frameCount > 0){
        this._frameCount--;
    }
}
SpriteCharaImg.prototype.updateSubImag = function (){
    if (!this.visible) return;
    if (!this._updateNum) return;

    if (this.waitFrameCount()){
        this.updateFrameCount();
        return;
    }
    
    this.children.forEach(function (sp){
        sp.visible = false;
        if (this.playNum() == sp._id){
            sp.visible = true;
        }
    }, this)
    this.addPlayNum();
    this.resetFrameCount();

}

// 地圖圖片
function Sprite_MapImage() {
    this.initialize.apply(this, arguments);
}
Sprite_MapImage.prototype = Object.create(Sprite.prototype);
Sprite_MapImage.prototype.constructor = Sprite_MapImage;
Sprite_MapImage.prototype.initialize = function (bitmap){
    Sprite.prototype.initialize.call(this, bitmap);
}
Sprite_MapImage.prototype.screenX  = function (){
    let tw = $gameMap.tileWidth();
    return Math.round(this.scrolledX() * tw);
}
Sprite_MapImage.prototype.screenY = function(){
    let th = $gameMap.tileHeight();
    return Math.round(this.scrolledY() * th);		
}
Sprite_MapImage.prototype.scrolledX = function(){
	return $gameMap.adjustX(0)
}
Sprite_MapImage.prototype.scrolledY = function (){
	return $gameMap.adjustY(0)
}
Sprite_MapImage.prototype.update = function  (){
	this.x = this.screenX();
	this.y = this.screenY();
}

function log (str){
    console.log(str)
}