//=============================================================================
// Chimaki_AutoBattleCommand.js
// Version: 1.0
//=============================================================================
/*:
* @plugindesc 加入全自動戰鬥
* @author Chimaki 
*
* @param openAuto
* @desc 是否開啟自動戰鬥選項
* @default 8
* 
* @param autoText
* @desc 自動戰鬥文字
* @default 自動戰鬥
* 
* ============================================================================
* @help
* 本插件提供基本 自動戰鬥選項
* 更多RMMV教學文章 都在Maker製造機: www.chimakier.com
*/
//=============================================================================

var Imported = Imported || {};
Imported.Chimaki_AutoBattle = [];

(function(){   
	var Chimaki_parameters = PluginManager.parameters('Chimaki_AutoBattleCommand');
	var autoBattle = Imported.Chimaki_AutoBattle;

	autoBattle.switches = false;
	autoBattle.isOpen = Math.floor(Chimaki_parameters['openAuto']|| 8);
	autoBattle.string = String(Chimaki_parameters['autoText'] || '自動戰鬥');
// ============================================================================
// plugin start
//=============================================================================
// ============================================================================
// obj
//=============================================================================

	Game_BattlerBase.prototype.isAutoBattle = function() {
		if (autoBattle.switches){
			return 1;
		}
    	return this.specialFlag(Game_BattlerBase.FLAG_ID_AUTO_BATTLE);
	};	
	Window_PartyCommand.prototype.makeCommandList = function() {
	    this.addCommand(TextManager.fight,  'fight');
	    if ($gameSwitches.value(autoBattle.isOpen)){
	    	this.addCommand(autoBattle.string,  'autoBattle');
		}
	    this.addCommand(TextManager.escape, 'escape', BattleManager.canEscape());

	};	

// ============================================================================
// Scene_battle
//=============================================================================
	Scene_Battle.prototype.initialize = function() {
	    Scene_Base.prototype.initialize.call(this);
	    autoBattle.switches = false;
	};
	var chimaki_scene_auto_battle = Scene_Battle.prototype.createPartyCommandWindow;
	Scene_Battle.prototype.createPartyCommandWindow = function() {
		chimaki_scene_auto_battle.call(this);
		if ($gameSwitches.value(autoBattle.isOpen)){
			this._partyCommandWindow.setHandler('autoBattle',  this.commandAllAuto.bind(this));
		}
	    
	};
	Scene_Battle.prototype.commandAllAuto = function(){
		autoBattle.switches = true;
	    $gameParty.members().forEach(function(actor) {
	      if (actor.canMove() && actor._actionState  === 'undecided') {
	        actor.makeAutoBattleActions();
	      }
	    });
	    this.endCommandSelection();
	    BattleManager.startTurn();

	}



}());


