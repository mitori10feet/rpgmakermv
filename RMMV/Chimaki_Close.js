


//=============================================================================
// Chimaki_Close.js
// Version: 1.0
//=============================================================================
/*
 * @plugindesc for 巴哈網友 - 關閉部分UI 顯示
 * @author Chimaki
 *
 * @param 
 * @desc 
 * @default 0
 *
 * @help 作者網站 Maker 製造機 http://www.chimakier.com	
 */
(function (){

	/*
	* 這邊是關閉menu中的道具
	*
	*
	*/
	Scene_Menu.prototype.createGoldWindow = function() {
	    this._goldWindow = new Window_Gold(0, 0);
	    this._goldWindow.y = Graphics.boxHeight - this._goldWindow.height;
	    // this.addWindow(this._goldWindow);
	};



	/*
	* 這邊是只留 道具類別
	*
	*
	*/
	Window_ItemCategory.prototype.makeCommandList = function() {
	    this.addCommand(TextManager.item,    'item');
	    // this.addCommand(TextManager.weapon,  'weapon');
	    // this.addCommand(TextManager.armor,   'armor');
	    // this.addCommand(TextManager.keyItem, 'keyItem');
	};	

	/*
	* 這邊是關閉menu中hp/ mp / tp
	*
	*
	*/
	Window_Base.prototype.drawActorSimpleStatus = function(actor, x, y, width) {
	    var lineHeight = this.lineHeight();
	    var x2 = x + 180;
	    var width2 = Math.min(200, width - 180 - this.textPadding());
	    this.drawActorName(actor, x, y);
	    this.drawActorLevel(actor, x, y + lineHeight * 1);
	    this.drawActorIcons(actor, x, y + lineHeight * 2);
	    this.drawActorClass(actor, x2, y);
	    /* 如果想顯示 hp 請拔掉下方雙斜線*/
	    // this.drawActorHp(actor, x2, y + lineHeight * 1, width2); 

	    /* 如果想顯示 mp 請拔掉下方雙斜線*/
	    // this.drawActorMp(actor, x2, y + lineHeight * 2, width2);
	};

	Window_Status.prototype.drawBasicInfo = function(x, y) {
	    var lineHeight = this.lineHeight();
	    this.drawActorLevel(this._actor, x, y + lineHeight * 0);
	    this.drawActorIcons(this._actor, x, y + lineHeight * 1);
	    // this.drawActorHp(this._actor, x, y + lineHeight * 2);
	    // this.drawActorMp(this._actor, x, y + lineHeight * 3);
	};
	Window_BattleStatus.prototype.drawGaugeAreaWithTp = function(rect, actor) {
	    // this.drawActorHp(actor, rect.x + 0, rect.y, 108);
	    // this.drawActorMp(actor, rect.x + 123, rect.y, 96);
	    // this.drawActorTp(actor, rect.x + 234, rect.y, 96);
	};
	Window_BattleStatus.prototype.drawGaugeAreaWithoutTp = function(rect, actor) {
	    // this.drawActorHp(actor, rect.x + 0, rect.y, 201);
	    // this.drawActorMp(actor, rect.x + 216,  rect.y, 114);
	};






})();