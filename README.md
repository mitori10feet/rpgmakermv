# Maker製造機自製插件

## 使用規範
- 插件為自由使用
- 若遊戲中使用插件，請註明Maker製造機粉專 https://www.facebook.com/chimakier
- 若改造插件再次發布 請註明此處gitlab為原始網址

## for_categakuen 資料夾
- 此處為替貓咪學園客製插件

## RMMV 資料夾
- 此處為一般自製插件 for mv

## RMMZ 資料夾
- 此處為一般自製插件for mz

## 作者Chimaki
- [作品：眼中的世界 ](https://store.steampowered.com/app/811070)
- [粉絲專頁：Maker製造機](https://www.facebook.com/chimakier)
- [Patreon：Maker製造機Patreon](https://www.patreon.com/chimakier)
