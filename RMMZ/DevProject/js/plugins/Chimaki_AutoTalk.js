//=============================================================================
// RPG Maker MZ Chimaki_AutoTalk
// Version: 1.0
//=============================================================================
/*:
 * @target MZ
 * @plugindesc 
 * @author Chimaki
 * @url http://www.chimakier.com
 * 
 * @param autoWait
 * @text 每個字元對應等待時間
 * @desc 每個字元對應等待時間
 * @type number
 * @default 0.1
 * 
 * 
 * @command switch
 * @text 設定自動對話是否開啟
 * @desc msg center one times
 * 
 * @arg switch
 * @type boolean
 * @default false
 * 
 * @help
 * 插件指令開啟後，將開啟自動對話。自動對話時間 = 字元 * autoWait，時間到則會自動切換到下一個事件
 * 即使開啟自動對話，也可以按下確認鍵前往下一頁
 * 
 * 建議配合圖片事件的插件來達到讓玩家設定自動開啟功能
 * EX: 點xxx圖片呼叫公用事件來開啟自動對話功能
 */

'use strict'
var alias = alias || {};
(() => {
    let _active = false;
    const frame = 60;
    const pluginName = 'Chimaki_AutoTalk';
    const autoWait = Number(PluginManager.parameters(pluginName)['autoWait']);
    PluginManager.registerCommand(pluginName, "switch", args => {     
        _active = args.switch == "true" ? true : false;
    });
    alias.window_msg_initMember = Window_Message.prototype.initMembers;
    Window_Message.prototype.initMembers = function() {
        this._autoWait = 0 ;
        alias.window_msg_initMember.call(this);
    };
    alias.window_msg_newPage = Window_Message.prototype.newPage
    Window_Message.prototype.newPage = function(textState) {
        alias.window_msg_newPage.call(this, textState);
        this._autoWait = textState.text.length * autoWait * frame;    

    };    
    /**
     * @override
     */
    Window_Message.prototype.updateInput = function() {
        if (this.isAnySubWindowActive()) {
            return true;
        }
        if (this.pause && !_active) {
            if (this.isTriggered()) {
                this.updateNextPage();
            }
            return true;
        }
        else if (this.pause && _active) {
            /** 點擊仍然讓他往下一句 */
            if (this.isTriggered()) {
                this.updateNextPage();
                return true;
            }
            /** 沒有點擊就等待 */
            if (this._autoWait > 0) {                
                this._autoWait--;
                return true;
            }
            if (this._autoWait <= 0) {
                this._autoWait = 0;
            }
            this.updateNextPage();
            return true;
        }
        return false;
    };    
    Window_Message.prototype.updateNextPage = function (){
        Input.update();
        this.pause = false;
        if (!this._textState) {
            this.terminateMessage();
        }        
    }

})();
