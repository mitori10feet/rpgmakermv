//=============================================================================
// RPG Maker MZ - Chimaki Lang
//=============================================================================
/*:
 * @target MZ
 * @plugindesc Configure multilingual by using CSV file v1.2
 * @author Chimaki
 *
 * @command chageLang
 * @text change lang by command
 * @desc change lang by command
 * 
 * @arg lang
 * @type string
 * @default tw
 * @text input lang
 * @desc input lang
 * 
 * 
 * 
 * @param defaultLang
 * @text default language
 * @type string
 * @default tw
 *
 * @param allLang
 * @text Supported languages
 * @type array
 * @default ['tw','cn','en']
 * 
 * 
 * @param allLangShow
 * @text Supported languages
 * @type array
 * @default ['繁體中文','簡體中文','English']
 * 
 * 
 * @param showOnOption
 * @text option text
 * @type string
 * @default selectLang
 * 
 * @param loadfileName
 * @text csv file name
 * @type array
 * @default ['UI']
 * 
 * @help Chimaki_Lang.js
 * itch : https://conviction-srpg.itch.io/
 * webside : https://www.chimakier.com
 * 
 * Preparation:
 * Create a folder named csv under the project
 * 
 * Instructions:
 * 1. Enter \T[Text-number-in-CSV-file] in Event Commands to call the multilingual system.
 * 2. Enter \T[Text-number-in-CSV-file] in Database to call the multilingual system.
 * 
 * P.S. You can use Excel 2019 or above to save as CSV files that use UTF-8 character encoding by clicking the CSV UTF-8 (Comma delimited) from Save as type menu. And download as a CSV UTF-8 (Comma delimited) file from Google Drive is also recommended.
* Special thanks to: Eagle, 快閃小強
 * if you want add new lang call "jp", add Column
 * 
 * ex: id , who, tw, cn ,en , jp
 * 
 * ps. the who row just for user to note some thing
 * 
 * how to use 
 * 1. you can input \T[your_code] in text event/ name / chioce...
 * 2. you can input \T[your_code] into data base , ex: item description
 * 
 * P.S. if you use exl to write your csv table, dont forget export code to utf-8
 * P.S. You can use Excel 2019 or above to save as CSV files that use UTF-8 character 
 * encoding by clicking the CSV UTF-8 (Comma delimited) from Save as type menu. 
 * And download as a CSV UTF-8 (Comma delimited) file from Google Drive is also recommended.
 * 
 * Special thanks to：Eagle/快閃小強
 */

/*:zh_TW
 * @target MZ
 * @plugindesc 多語系MZ進化版v1.1
 * @author Chimaki
 * 
 * 
 * @command chageLang
 * @text 修改語系
 * @desc 修改語系
 * 
 * @arg lang
 * @type string
 * @default tw
 * @text 輸入要切換的語系
 * @desc 輸入要切換的語系
 * 
 * @param defaultLang
 * @text 預設語系(對應csv上的設定)
 * @type string
 * @default tw
 *
 * @param allLang
 * @text 你要支援的語系(對應csv上的設定)
 * @type array
 * @default ['tw','cn','en']
 * 
 * 
 * @param allLangShow
 * @text 各個語系在選項中的顯示名稱
 * @type array
 * @default ['繁體中文','簡體中文','English']
 * 
 * 
 * @param showOnOption
 * @text 此功能在選單中顯示的名稱
 * @type string
 * @default 選擇語言
 * 
 * @param loadfileName
 * @text 你要讀取的csv 檔案名稱
 * @type array
 * @default ['UI'] 
 * 
 * @help Chimaki_Lang.js
 * itch : https://conviction-srpg.itch.io/
 * webside : https://www.chimakier.com
 * 
 * how to use 
 * 事前準備：
 * 專案底下新增 csv 資料夾
 * 第一列的格式分別為 id,who(使用者用來備註用), tw ,en ... 從tw開始，可以自由新增語系欄位
 * ex： 假設你想要新增一個語言叫做 jp, 你的第一列就會變成
 * id , who , tw, en ,jp ...
 * 
 * 別忘也要到插件參數中新增jp 對應的文字
 * 
 * 使用：
 * 1. 在對話事件中輸入 \T[你的文本編號] 即可自動轉換成多語系
 * 2. 在資料庫中輸入 \T[你的文本編號] 即可自動轉換成多語系
 * 
 * P.S. 如果使用exl輸出csv, 請注意編碼要是utf8(輸出後用txt打開即可確認), 建議使用百度雲or goolge drive 
 * 下載成csv檔案，編碼就不會有問題
 * 
 * 若使用exl 輸出請按照下方流程
 * 1. 另存新檔
 * 2. web 選項
 * 3  到編碼中選則utf-8 按下確認
 * 4. 輸出成 CSV(*.csv utf-8)
 *
 * 特別感謝：Eagle/快閃小強
 */

/*:zh_CN
 * @target MZ
 * @plugindesc 多語系MZ進化版v1.1
 * @author Chimaki
 * 
 * @command chageLang
 * @text 修改語系
 * @desc 修改語系
 * 
 * @arg lang
 * @type string
 * @default tw
 * @text 輸入要切換的語系
 * @desc 輸入要切換的語系
 * 
 * 
 * @param defaultLang
 * @text 預設語系(對應csv上的設定)
 * @type string
 * @default tw
 *
 * @param allLang
 * @text 你要支援的語系(對應csv上的設定)
 * @type array
 * @default ['tw','cn','en']
 * 
 * 
 * @param allLangShow
 * @text 各個語系在選項中的顯示名稱
 * @type array
 * @default ['繁體中文','簡體中文','English']
 * 
 * 
 * @param showOnOption
 * @text 此功能在選單中顯示的名稱
 * @type string
 * @default 選擇語言
 * 
 * @param loadfileName
 * @text 你要讀取的csv名稱
 * @type array
 * @default ['UI']
 * 
 * @help Chimaki_Lang.js
 * itch : https://conviction-srpg.itch.io/
 * webside : https://www.chimakier.com
 * 
 * 事前準備：
 * 專案底下新增 csv 資料夾
 * 第一列的格式分別為 id,who(使用者用來備註用), tw ,en ... 從tw開始，可以自由新增語系欄位
 * ex： 假設你想要新增一個語言叫做 jp, 你的第一列就會變成
 * id , who , tw, en ,jp ...
 *  
 * 別忘也要到插件參數中新增jp 對應的文字
 * 
 * 使用：
 * 1. 在對話事件中輸入 \T[你的文本編號] 即可自動轉換成多語系
 * 2. 在資料庫中輸入 \T[你的文本編號] 即可自動轉換成多語系
 * 
 * P.S. 如果使用exl輸出csv, 請注意編碼要是utf8(輸出後用txt打開即可確認), 建議使用百度雲or goolge drive 
 * 下載成csv檔案，編碼就不會有問題
 * 
 * 若使用exl 輸出請按照下方流程
 * 1. 另存新檔
 * 2. web 選項
 * 3  到編碼中選則utf-8 按下確認
 * 4. 輸出成 CSV(*.csv utf-8)
 * 
 * 特別感謝：Eagle/快閃小強
 * 
 */

"use strict";


ConfigManager.lang = 'tw';

var srpgModuleAlias = srpgModuleAlias || {}
srpgModuleAlias.configManager = srpgModuleAlias.configManager || {};
srpgModuleAlias.windowOptions = srpgModuleAlias.windowOptions || {};


const LangpluginName = "Chimaki_Lang";
const chimaki_params = PluginManager.parameters(LangpluginName);
const defaultLang = chimaki_params["defaultLang"] || 'tw';
const LANG_LIST = eval(chimaki_params["allLang"]);
const LANG_SHOW = eval(chimaki_params["allLangShow"]);
var LOAD_CSV = eval(chimaki_params["loadfileName"]);
const OPTION_TEXT = (chimaki_params["showOnOption"]) || '語言選擇';

LOAD_CSV.push('ch_000');
PluginManager.registerCommand(LangpluginName, "chageLang", args => {
    const lang = args.lang;
    ConfigManager.lang = lang;


});



console.error(`chimaki `, LANG_LIST)
var langMap = {};
LANG_LIST.forEach((lang, index) => {
    langMap[`${lang}`] = LANG_SHOW[`${index}`];
})






/**
 * 語言專用
 */
class LangManager {
    static _data = {};
    static _loadedCsv = {}
    static get loadedCsv() {
        return this._loadedCsv
    }
    static set loadedCsv(csv) {
        this._loadedCsv = csv;
    }


    static get data() {
        return this._data || {};
    }
    static set data(value) {

        for (let key in value) {
            this._data[key] = value[key];
        }
    }
    static loadCsv(filename) {
        console.error(`chimaki load csv ${filename}`)
        if (!filename) {
            console.warn('no file name')
            return;
        }
        filename = filename
        /** parse   */
        AssetsManager.loadCsv(filename, true).then(data => {
            let mergeHash = SubUtil.mergeHash(this.data, data)
            this.data = mergeHash;
            let csvlist = LangManager.loadedCsv;
            csvlist[filename] = 1;
            LangManager.loadedCsv = csvlist;

        }).catch(e => {
            console.error(e);
        })

    }
    static name(index) {
        let lang = ConfigManager.lang;
        return this.data[lang][`name_${index}`];
    }
    static text(index) {
        // return `${index}`;
        let lang = ConfigManager.lang;
        return this.data[lang][index];
    };
}



srpgModuleAlias.configManager.makerData = ConfigManager.makeData;
ConfigManager.makeData = function (config) {
    config = srpgModuleAlias.configManager.makerData.call(this, config);
    config = config || {};
    config.showBattle = this.showBattle;
    config.lang = this.lang;
    return config;

};

srpgModuleAlias.configManager.applyData = ConfigManager.applyData;
ConfigManager.applyData = function (config) {
    srpgModuleAlias.configManager.applyData.call(this, config);
    this.lang = this.readLang(config);
    this.showBattle = this.readFlag(config, "showBattle", true);
};

ConfigManager.readLang = function (config) {
    return config['lang'] || 'tw';
};

/**
 * window options
 */

srpgModuleAlias.windowOptions.makeCommandList = Window_Options.prototype.makeCommandList;
Window_Options.prototype.makeCommandList = function () {
    srpgModuleAlias.windowOptions.makeCommandList.call(this);
    this.addLangOptions()
};




/**
 * new 
 */
Window_Options.prototype.addLangOptions = function () {
    this.addCommand(OPTION_TEXT, "lang");
}
Window_Options.prototype.langStatusText = function (current) {
    return langMap[`${current}`];
};
Window_Options.prototype.isLangSymbol = function (symbol) {
    return symbol == "lang";
}
/**
 * @todo 用index 處理next lang
 */
Window_Options.prototype.changeLang = function (symbol, add) {
    let nowLang = ConfigManager[symbol];
    let index = LANG_LIST.indexOf(nowLang);
    index += add ? 1 : -1;
    if (index >= LANG_LIST.length) {
        index = LANG_LIST.length - 1;
    }
    if (index < 0) index = 0;
    this.changeValue(symbol, LANG_LIST[index]);

}


Window_Options.prototype.statusText = function (index) {
    const symbol = this.commandSymbol(index);
    const value = this.getConfigValue(symbol);
    if (this.isVolumeSymbol(symbol)) {
        return this.volumeStatusText(value);
    } else if (this.isLangSymbol(symbol)) {
        return this.langStatusText(value);
    } else {
        return this.booleanStatusText(value);
    }
};
Window_Options.prototype.getConfigValue = function (symbol) {
    return ConfigManager[symbol];
};

Window_Options.prototype.processOk = function () {
    const index = this.index();
    const symbol = this.commandSymbol(index);
    if (this.isVolumeSymbol(symbol)) {
        this.changeVolume(symbol, true, true);
    } else if (this.isLangSymbol(symbol)) {
        this.changeLang(symbol, true);
    } else {
        this.changeValue(symbol, !this.getConfigValue(symbol));
    }
};

Window_Options.prototype.cursorRight = function () {
    const index = this.index();
    const symbol = this.commandSymbol(index);
    if (this.isVolumeSymbol(symbol)) {
        this.changeVolume(symbol, true, false);
    } else if (this.isLangSymbol(symbol)) {
        this.changeLang(symbol, true);
    } else {
        this.changeValue(symbol, true);
    }
};

Window_Options.prototype.cursorLeft = function () {
    const index = this.index();
    const symbol = this.commandSymbol(index);
    if (this.isVolumeSymbol(symbol)) {
        this.changeVolume(symbol, false, false);
    } else if (this.isLangSymbol(symbol)) {
        this.changeLang(symbol, false);
    } else {
        this.changeValue(symbol, false);
    }
};





/**
 * 以下是語系處理
 */

/**
 * @override 處理字串
 * @param 
 */
Game_Message.prototype.add = function (text) {
    this._texts.push(this.processLangChange(text));
};

Game_Message.prototype.processLangChange = function (text) {
    return checkLangText(text);
}
/**
 * 頭圖名字多語系
 */
Game_Message.prototype.setSpeakerName = function (speakerName) {
    this._speakerName = checkLangText(speakerName)
};

/**
 * @override 處理選項
 * @param {} params 
 */
Game_Interpreter.prototype.setupChoices = function (params) {
    const choices = params[0].clone();
    for (let i = 0; i < choices.length; i++) {
        choices[i] = $gameMessage.processLangChange(choices[i]);
    };
    const cancelType = params[1] < choices.length ? params[1] : -2;
    const defaultType = params.length > 2 ? params[2] : 0;
    const positionType = params.length > 3 ? params[3] : 2;
    const background = params.length > 4 ? params[4] : 0;
    $gameMessage.setChoices(choices, defaultType, cancelType);
    $gameMessage.setChoiceBackground(background);
    $gameMessage.setChoicePositionType(positionType);
    $gameMessage.setChoiceCallback(n => {
        this._branch[this._indent] = n;
    });
};


Window_Base.prototype.drawTextEx = function (text, x, y, width) {
    text = checkLangText(text);
    this.resetFontSettings();
    const textState = this.createTextState(text, x, y, width);
    this.processAllText(textState);
    return textState.outputWidth;
};

Window_Help.prototype.setText = function (text) {
    text = checkLangText(text);
    if (this._text !== text) {
        this._text = text;
        this.refresh();
    }
};
Bitmap.prototype.drawText = function (text, x, y, maxWidth, lineHeight, align) {
    text = checkLangText(text);

    const context = this.context;
    const alpha = context.globalAlpha;
    maxWidth = maxWidth || 0xffffffff;
    let tx = x;
    let ty = Math.round(y + lineHeight / 2 + this.fontSize * 0.35);
    if (align === "center") {
        tx += maxWidth / 2;
    }
    if (align === "right") {
        tx += maxWidth;
    }
    context.save();
    context.font = this._makeFontNameText();
    context.textAlign = align;
    context.textBaseline = "alphabetic";
    context.globalAlpha = 1;
    this._drawTextOutline(text, tx, ty, maxWidth);
    context.globalAlpha = alpha;
    this._drawTextBody(text, tx, ty, maxWidth);
    context.restore();
    this._baseTexture.update();
};


function checkLangText(text) {

    if (!text) return text;
    text += "";
    text = text.replace(/\\/g, "\x1b");
    text = text.replace(/\x1b\x1b/g, "\\");

    text = text.replace(/\x1bT\[(\w+)\]/gi, (_, p1) => {
        return LangManager.text(p1);
    });
    return text;

}


/**
 * 讀取資源取消fs 
 */
class AssetsManager {

    static loadStage(url) {
        return new Promise((res, rej) => {
            const xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.overrideMimeType("application/json;charset=utf-8");
            xhr.onload = (e) => {
                let json = xhr.responseText;
                res(json);
            }
            xhr.onerror = (e) => {
                rej(e);
                console.error(`load stage error`, e);
            }
            xhr.send();
        })

    }

    static loadCsv(url, isLang = false) {
        return new Promise((res, rej) => {
            const xhr = new XMLHttpRequest();
            xhr.open("GET", `./csv/${url}.csv`, true);
            xhr.overrideMimeType("text/csv;charset=utf-8");
            xhr.onload = (e) => {
                let text = xhr.responseText;
                let map = isLang ? this.parseLang(text) : this.parseCsv(text);
                res(map)
            }
            xhr.onerror = (e) => {
                rej(e)
            }
            xhr.send();
        })
    }
    static parseCsv(text) {
        let map = {};
        let mapping = {};

        let arr = text.split(/\r/);
        let header = arr.shift().split(",");

        for (let i = 0; i < arr.length; i++) {
            /** insert key */
            let l = arr[i].split(',')
            let key = l[0].trim();
            map[key] = {};
        }

        for (let i = 0; i < header.length; i++) {
            let key = header[i].trim();
            mapping[key] = i;
        }

        for (let i = 0; i < arr.length; i++) {
            let arr2 = arr[i].split(',');
            for (let j = 0; j < arr2.length; j++) {
                let d = arr2[j].trim();

                if (j == 0 && map[d]) {
                    for (let k in mapping) {
                        if (arr2[0].trim() === arr2[mapping['id']].trim())
                            map[d][k] = arr2[mapping[k]].trim();
                    }

                }
            }
        }
        return map;

    }
    static parseLang(text) {
        let map = {}
        LANG_LIST.forEach(lang => {
            map[`${lang}`] = {};
        })

        let mapping = {}
        LANG_LIST.forEach(lang => {
            mapping[`${lang}`] = LANG_LIST.indexOf(lang) + 2;
        })
        let arr = text.split(/\r/);
        for (let i = 1; i < arr.length; i++) {
            let arr2 = arr[i].split(',');
            for (let j = 0; j < arr2.length; j++) {
                let str = arr2[j].trim();

                /** 所有key */
                if (j == 0) {
                    for (let k in map) {
                        if (mapping[k]) {
                            let d = mapping[k]
                            let t2 = arr2[d];
                            /** 如果換行則去頭去尾，因為一定會有queto */
                            if (arr2[d] && arr2[d].split("\n").length >= 2) {
                                let len = t2.length;
                                t2 = t2.substr(1, len - 2)
                            }
                            map[k][str] = t2;
                        }
                    }
                }
            }
        }

        return map;
    }

}

LOAD_CSV.forEach(name => {
    LangManager.loadCsv(name)
})



/** 以下恢復擴充 */
Scene_Options.prototype.maxCommands = function () {
    return 5;
};

Window_Options.prototype.addGeneralOptions = function () {
    // this.addCommand(TextManager.alwaysDash, "alwaysDash");
    // this.addCommand(TextManager.commandRemember, "commandRemember");
    this.addCommand(TextManager.touchUI, "touchUI");
    this.addCommand('戰鬥顯示', "showBattle");
};

Window_Options.prototype.addVolumeOptions = function () {
    this.addCommand(TextManager.bgmVolume, "bgmVolume");
    this.addCommand(TextManager.seVolume, "seVolume");
};