//=============================================================================
// RPG Maker MZ Chimaki_ChangePartyLeader
// Version: 1.0
//=============================================================================
/*:
 * @target MZ
 * @plugindesc 
 * @author Chimaki
 * @url http://www.chimakier.com
 * 
 * @param changeLeaderKey
 * @text 切換隊長的熱鍵號碼 (正向切換)
 * @desc 設定快速切換的熱鍵號碼，請用https://keycode.info/ 查詢對應號碼 (預設為鍵盤S)
 * @type number
 * @default 83
 * 
 * @param changeLeaderKeyR
 * @text 切換隊長的熱鍵號碼(反向切換)
 * @desc 設定快速切換的熱鍵號碼，請用https://keycode.info/ 查詢對應號碼 (預設為鍵盤D)
 * @type number
 * @default 68
 * 
 * @param changeEvent
 * @text 正向切換隊長觸發的公用事件id
 * @desc 通常用在音效
 * @type number
 * @default 3
 * 
 * @param changeEventR
 * @text 反向切換隊長觸發的公用事件
 * @desc 通常用在音效
 * @type number
 * @default 4
 *
 *
 * @command switchChange
 * @text 是否開啟此功能
 * 
 * @arg switch
 * @type boolean
 * @default true
 * 
 * @help
 * 設定熱鍵快速切換地圖上的隊長，戰鬥順序與Menu順序也會一起改變
 * 
 */
// * @command addWeight
// * @text add max weight
// * @desc add max weight
var alias = alias || {};
(() => {
    'use strict'
    const pluginName = 'Chimaki_ChangePartyLeader';
    /** 過重顯示 */
    const changeLeaderKey = Number(PluginManager.parameters(pluginName)['changeLeaderKey']);
    const changeLeaderKeyR = Number(PluginManager.parameters(pluginName)['changeLeaderKeyR']);
    const changeEvent = Number(PluginManager.parameters(pluginName)['changeEvent']);
    const changeEventR = Number(PluginManager.parameters(pluginName)['changeEventR']);    
    let lastIndex = 0;
    let _active = true;
    PluginManager.registerCommand(pluginName, "switchChange", args => {     
        _active = args.switch == "true" ? true : false;
    });

    alias.changeLeaderKeyDown = SceneManager.onKeyDown;
    SceneManager.onKeyDown = function(event) {
        alias.changeLeaderKeyDown.call(this, event);
        if (!event.ctrlKey && !event.altKey) {
            console.log(`avtive ${_active}`)
            if (!_active) return;
            if (event.keyCode === changeLeaderKey) {   
                lastIndex++;
                $gameTemp.reserveCommonEvent(changeEvent);
                $gamePlayer.refresh();
            } 
            if (event.keyCode === changeLeaderKeyR) {
                lastIndex--;                
                $gameTemp.reserveCommonEvent(changeEventR);
                $gamePlayer.refresh();                
            }
        }
    }      
    Game_Party.prototype.battleMembers = function() {
        let len = this.maxBattleMembers();
        let arr = this.allMembers()
        .slice(0, len)
        .filter(actor => actor.isAppeared());
        lastIndex = lastIndex >= len  ?  0 : lastIndex;
        lastIndex = lastIndex < 0 ? len - 1 : lastIndex;
        let tempCount = len - lastIndex;
        let tempArr = arr.splice(lastIndex, tempCount);    
        tempArr = tempArr.concat(arr);
        return tempArr;


    };
    Game_Party.prototype.members = function() {
        return this.battleMembers();
    };

})();
