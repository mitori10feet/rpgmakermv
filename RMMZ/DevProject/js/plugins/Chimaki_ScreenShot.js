//=============================================================================
// RPG Maker MZ Chimaki_ScreenShot
// Version: 1.0
//=============================================================================
/*:
 * @target MZ
 * @plugindesc key donw "P" , get screenshot
 * @author Chimaki
 * 
 * @param message
 * @desc set take screenshot msg
 * @type string
 * @default take screenshot to  :
 * 
 * 
 * 
*/

var alias = alias || {};
alias.screenTool = alias.screenTool || {};
(() => {

    'use strict'
    const pluginName = 'Chimaki_ScreenShot';
    const message = String(PluginManager.parameters(pluginName)['message']);


    function shot() {
        SceneManager.setGameShot();


        let now =  Date.now();
        let fs = require('fs');
            let path = require('path');
            let base = path.dirname(process.mainModule.filename);
            path = path.join(base, 'screenshot/');
        
        let bitmap = SceneManager.getGameShot();;
        
        let urlData = bitmap.canvas.toDataURL('png',100);		
        let base64Data = urlData.replace( (/^data:image\/png;base64,/) , "");
        let last_path = path ;

        if (!fs.existsSync(last_path)) {
            fs.mkdirSync(last_path);
        }
        
        let str =  last_path + now ;
        fs.writeFileSync(str + ".png", base64Data, 'base64');
        SceneManager.clearShot();	
        
        alert(message + 'screenshot/' );
    }

    SceneManager.clearShot = function (){
        this._temp_shop = null;
    }
    SceneManager.getGameShot = function() {
        if (!this._temp_shop) this.setGameShot();
        return this._temp_shop;    
    };
    SceneManager.setGameShot = function (){
        this._temp_shop = Bitmap.snap(this._scene);
    }
    alias.screenTool = SceneManager.onKeyDown;
    SceneManager.onKeyDown = function(event) {
        alias.screenTool.call(this, event);
        if (!event.ctrlKey && !event.altKey) {
            switch (event.keyCode) {
                case 80:
                    shot();
                    break;                            
            }                
        }
    }

})()