//=============================================================================
// RPG Maker MZ Chimaki_SubTool
// Version: 1.0
//=============================================================================
/*:zh_TW
 * @target MZ
 * @plugindesc 快速存檔/讀取,自動對話,切換隊長,一次文字置中的小工具,8方向移動
 * @author Chimaki
 * @url https://www.patreon.com/chimakier
 * 
 * 
 * @param dirSwitch
 * @text 開啟8方向移動
 * @type boolean
 * @default false
 * 
 * 
 * @param openShot
 * @type boolean
 * @text 拍照功能，預設為false
 * @desc 使用P進行拍照
 * @default false
 * 
 * @param message
 * @desc 拍照後顯示的字串
 * @type string
 * @default take screenshot to  :
 * @parent openShot
 * 
 * 
 * @command toCenter
 * @text 一次性文字置中
 * 
 * 
 * @param openFastSaveLoad
 * @type boolean
 * @text 快速存檔/讀取檔案
 * @desc 是否開啟相關功能
 * @default false
 * 
 * @param fastSave
 * @desc 設定快速存擋的熱鍵號碼，請用https://keycode.info/ 查詢對應號碼 (預設為鍵盤S)
 * @type number
 * @default 83
 * @parent openFastSaveLoad
 * 
 * @param saveMsg
 * @desc 快速存擋後呼叫公用事件的事件id
 * @type number
 * @default 1
 * @parent openFastSaveLoad 
 * 
 * @param saveAutoSave
 * @desc 設定為true: 讀取時，只儲存快速存檔
 * @type boolean
 * @default false
 * @parent openFastSaveLoad
 * 
 *  
 * @param loadAutoSave
 * @desc 設定為true: 讀取時，只讀取快速存檔
 * @type boolean
 * @default false
 * @parent openFastSaveLoad
 * 
 * 
 * @param fastLoad
 * @desc 設定快速讀擋的熱鍵號碼，(預設為鍵盤D)
 * @type number
 * @default 76
 * @parent openFastSaveLoad
 * 
 * @command switchAutoSave
 * @text 開關快速存檔功能
 * @desc 當你有些地圖不想讓玩家快速存檔/讀檔時使用 (快速存檔/讀取檔案設定為true才會使此指令生效)
 * 
 * @arg switch
 * @type boolean
 * @default true
 * 
 * @param openChangeLeader
 * @text 打開切換隊長相關功能
 * @type boolean
 * @desc 相關功能請用https://keycode.info/ 查詢對應號碼
 * @default false
 * 
 * 
 * @param changeLeaderKey
 * @text 切換隊長的熱鍵號碼 (正向切換)
 * @desc 設定快速切換的熱鍵號碼(預設為鍵盤Q)
 * @type numbe
 * @default 81
 * @parent openChangeLeader
 * 
 * @param changeLeaderKeyR
 * @text 切換隊長的熱鍵號碼(反向切換)
 * @desc 設定快速切換的熱鍵號碼，(預設為鍵盤W)
 * @type number
 * @default 87
 * @parent openChangeLeader
 * 
 * @param changeEvent
 * @text 正向切換隊長觸發的公用事件id
 * @desc 通常用在音效
 * @type number
 * @default 3
 * @parent openChangeLeader
 * 
 * @param changeEventR
 * @text 反向切換隊長觸發的公用事件
 * @desc 通常用在音效
 * @type number
 * @default 4
 * @parent openChangeLeader
 *
 *
 * @command switchChange
 * @text 是否開啟切換隊長
 * @desc 使用指令來打開/關閉切換隊長功能(打開切換隊長相關功能設定為true才會使此指令生效)
 * 
 * @arg switch
 * @type boolean
 * @default true
 * 
 * 
 * @param openAutoWait
 * @text 開啟自動對話相關功能
 * @type boolean
 * @desc 開啟後相關指令才會生效
 * @default false
 * 
 * @param autoWait
 * @text 每個字元對應等待時間
 * @desc 每個字元對應等待時間
 * @type number
 * @default 0.1
 * @parent openAutoWait
 * 
 * 
 * @command switch
 * @text 設定自動對話是否開啟
 * @desc 開啟自動對話相關功能-設定false可暫時關閉此功能
 * 
 * @arg switch
 * @type boolean
 * @default false
 * 
 */

'use strict'
var alias = alias || {};
const pluginName = "Chimaki_SubTool";

/**
 * 8方向移動
 */
(() => {

    let chimaki_params = PluginManager.parameters(pluginName);
    let dir_switch = chimaki_params['dirSwitch'] == "true" ? true : false;
    Game_Player.prototype.getInputDirection = function() {
        return dir_switch ?  Input.dir8 : Input.dir4;
    };
    Game_Map.prototype.xWithDirection = function(x, d) {
        return x + (d === 6 || d === 3 || d === 9 ? 1 : d === 1 || d === 7 || d === 4 ? -1 : 0);
    };
    
    Game_Map.prototype.yWithDirection = function(y, d) {
        return y + (d === 2 || d === 1 || d === 3 ? 1 : d === 8 || d === 7 || d === 9 ? -1 : 0);
    };
    
    Game_Map.prototype.roundXWithDirection = function(x, d) {        
        return this.roundX(x + (d === 6 || d === 3 || d == 9 ? 1 : d === 4 || d === 1 || d === 7 ? -1 : 0) );
    };
    
    Game_Map.prototype.roundYWithDirection = function(y, d) {
        return this.roundY(y + (d === 2 || d === 3 || d === 1 ? 1  : d === 8 || d === 7 || d === 9 ? -1 : 0));
    };

    // 讓移動時候圖片正常
    Sprite_Character.prototype.characterPatternY = function() {
        let dir = this._character.direction();

        if (dir == 2 || dir  == 1 || dir == 3){
            dir = 0
        }
        else if (dir == 4 ){
            dir = 1;
        }
        else if (dir == 6){
            dir = 2;
        }
        else if (dir == 8 || dir == 7 || dir == 9){
            dir = 3;
        }
        return dir;
    };    

})();


/**
 * 拍照工具
 */
alias.screenTool = alias.screenTool || {};
(() => {
    const openShot = PluginManager.parameters(pluginName)['openShot'] == "true" ? true : false;
    if (!openShot) return;
    const message = String(PluginManager.parameters(pluginName)['message']);
    function shot() {
        SceneManager.setGameShot();
        let now =  Date.now();
        let fs = require('fs');
            let path = require('path');
            let base = path.dirname(process.mainModule.filename);
            path = path.join(base, 'screenshot/');
        
        let bitmap = SceneManager.getGameShot();;
        
        let urlData = bitmap.canvas.toDataURL('png',100);		
        let base64Data = urlData.replace( (/^data:image\/png;base64,/) , "");
        let last_path = path ;

        if (!fs.existsSync(last_path)) {
            fs.mkdirSync(last_path);
        }
        
        let str =  last_path + now ;
        fs.writeFileSync(str + ".png", base64Data, 'base64');
        SceneManager.clearShot();	
        
        alert(message + 'screenshot/' );
    }

    SceneManager.clearShot = function (){
        this._temp_shop = null;
    }
    SceneManager.getGameShot = function() {
        if (!this._temp_shop) this.setGameShot();
        return this._temp_shop;    
    };
    SceneManager.setGameShot = function (){
        this._temp_shop = Bitmap.snap(this._scene);
    }
    alias.screenTool = SceneManager.onKeyDown;
    SceneManager.onKeyDown = function(event) {
        alias.screenTool.call(this, event);
        if (!event.ctrlKey && !event.altKey) {
            switch (event.keyCode) {
                case 80:
                    shot();
                    break;                            
            }                
        }
    }

})()

/**
 * 一次性文字置中工具
 */
alias.windowMsgTool = alias.windowMsgTool || {};
(() => {
    var center = false;
    PluginManager.registerCommand(pluginName, "toCenter", args => {
        center = true;
    });
    
    alias.windowMsgTool.msgWindowInitMember = Window_Message.prototype.initMembers;
    Window_Message.prototype.initMembers = function() {
        this._needCenter = 
        alias.windowMsgTool.msgWindowInitMember.call(this);
    };
    alias.windowMsgTool.msgWindowNewPage = Window_Message.prototype.newPage;
    Window_Message.prototype.newPage = function(textState) {
        alias.windowMsgTool.msgWindowNewPage.call(this, textState);
        if (this.needCenter()) {
            textState.x = this.contents.width / 2 - this.textWidth(this._textState.text) / 2;
            textState.y = (this.contents.height / 2) - (this._textState.height / 2);   
            center = false;
        }  
    };
    Window_Message.prototype.needCenter = function (){
        return center;
    }
})();

/**
 * 快速存檔功能
 */
alias.fastSave = alias.fastSave || {};
(() => {
    const openFastSaveLoad = PluginManager.parameters(pluginName)['openFastSaveLoad'] == "true" ? true : false;
    if (!openFastSaveLoad) return;

    var lastSave = 1;
    var lastLoad = 1;    
    const fastSave = Number(PluginManager.parameters(pluginName)['fastSave']);
    const fastLoad = Number(PluginManager.parameters(pluginName)['fastLoad']);
    const saveMsg =  Number(PluginManager.parameters(pluginName)['saveMsg']);
    const saveAutoSave = PluginManager.parameters(pluginName)['saveAutoSave'] == "true" ? true : false;
    const loadAutoSave = PluginManager.parameters(pluginName)['loadAutoSave'] == "true" ? true : false;

    let _active = true;
    PluginManager.registerCommand(pluginName, "switchAutoSave", args => {
        _active = args.switch == "true" ? true : false;
    });

    alias.fastSave = SceneManager.onKeyDown;
    SceneManager.onKeyDown = function(event) {
        alias.fastSave.call(this, event);
        let index = DataManager.latestSavefileId();
        if (!event.ctrlKey && !event.altKey) {
            /** 關閉則不做後續動作 */
            if (!_active) return;

            if (event.keyCode === fastSave) {   
                if ( SceneManager._scene instanceof Scene_Map){
                    if (saveAutoSave) index = 0;
                    this.executeSave(index);
                }
                
            } 
            if (event.keyCode === fastLoad) {
                if (loadAutoSave) index = 0;
                this.executeLoad(index);                
            } 
        }
    }
    SceneManager.executeSave = function(savefileId) {
        $gameSystem.setSavefileId(savefileId);
        $gameSystem.onBeforeSave();        
        DataManager.saveGame(savefileId)
            .then(() => this.onSaveSuccess())
            .catch(() => this.onSaveFailure());
    };
    SceneManager.onSaveSuccess = function() {
        SoundManager.playSave();   
        $gameTemp.reserveCommonEvent(saveMsg);
        
    };   
    SceneManager.onSaveFailure = function() {
        SoundManager.playBuzzer();
    };     

    SceneManager.executeLoad = function(savefileId) {
        const scene = SceneManager._scene;
        
        DataManager.loadGame(savefileId)
            .then(() => this.onLoadSuccess())
            .catch(() => this.onSaveFailure());
    }; 
    SceneManager.fadeSpeed = function() {
        return 24;
    };     
    SceneManager.onLoadSuccess = function () {
        const scene = SceneManager._scene;
        SoundManager.playLoad();
        scene.fadeOutAll();
        this.reloadMapIfUpdated();
        SceneManager.goto(Scene_Map);
        this._loadSuccess = true;
        const nowCcene = SceneManager._scene;

    }
    SceneManager.reloadMapIfUpdated = function() {
        if ($gameSystem.versionId() !== $dataSystem.versionId) {
            const mapId = $gameMap.mapId();
            const x = $gamePlayer.x;
            const y = $gamePlayer.y;
            $gamePlayer.reserveTransfer(mapId, x, y);
            $gamePlayer.requestMapReload();
        }
    };    
    Scene_Save.prototype.onSavefileOk = function() {
        Scene_File.prototype.onSavefileOk.call(this);
        const savefileId = this.savefileId();
        if (this.isSavefileEnabled(savefileId)) {
            this.executeSave(savefileId);
            lastSave = savefileId;
        } else {
            this.onSaveFailure();
        }
    };
    
    Scene_Load.prototype.onSavefileOk = function() {
        Scene_File.prototype.onSavefileOk.call(this);
        const savefileId = this.savefileId();
        if (this.isSavefileEnabled(savefileId)) {
            this.executeLoad(savefileId);
            lastLoad = savefileId;
        } else {
            this.onLoadFailure();
        }
    };
})();




(() => {
    const openChangeLeader = PluginManager.parameters(pluginName)['openChangeLeader'] == "true" ? true : false;
    if (!openChangeLeader) return;

    const changeLeaderKey = Number(PluginManager.parameters(pluginName)['changeLeaderKey']);
    const changeLeaderKeyR = Number(PluginManager.parameters(pluginName)['changeLeaderKeyR']);
    const changeEvent = Number(PluginManager.parameters(pluginName)['changeEvent']);
    const changeEventR = Number(PluginManager.parameters(pluginName)['changeEventR']);    
    let lastIndex = 0;
    let _active = true;
    PluginManager.registerCommand(pluginName, "switchChange", args => {     
        _active = args.switch == "true" ? true : false;
    });

    alias.changeLeaderKeyDown = SceneManager.onKeyDown;
    SceneManager.onKeyDown = function(event) {
        alias.changeLeaderKeyDown.call(this, event);
        if (!event.ctrlKey && !event.altKey) {
            console.log(`avtive ${_active}`)
            if (!_active) return;
            if (event.keyCode === changeLeaderKey) {   
                lastIndex++;
                $gameTemp.reserveCommonEvent(changeEvent);
                $gamePlayer.refresh();
            } 
            if (event.keyCode === changeLeaderKeyR) {
                lastIndex--;                
                $gameTemp.reserveCommonEvent(changeEventR);
                $gamePlayer.refresh();                
            }
        }
    }      
    Game_Party.prototype.battleMembers = function() {
        let len = this.maxBattleMembers();
        let arr = this.allMembers()
        .slice(0, len)
        .filter(actor => actor.isAppeared());
        lastIndex = lastIndex >= len  ?  0 : lastIndex;
        lastIndex = lastIndex < 0 ? len - 1 : lastIndex;
        let tempCount = len - lastIndex;
        let tempArr = arr.splice(lastIndex, tempCount);    
        tempArr = tempArr.concat(arr);
        return tempArr;
    };
    Game_Party.prototype.members = function() {
        return this.battleMembers();
    };

})();




/**
 * 自動對話功能
 */
(() => {
    const openAutoWait = PluginManager.parameters(pluginName)['openAutoWait'] == "true" ? true : false;
    if (!openAutoWait) return;
    let _active = false;
    const frame = 60;
    const autoWait = Number(PluginManager.parameters(pluginName)['autoWait']);
    PluginManager.registerCommand(pluginName, "switch", args => {     
        _active = args.switch == "true" ? true : false;
    });
    alias.window_msg_initMember = Window_Message.prototype.initMembers;
    Window_Message.prototype.initMembers = function() {
        this._autoWait = 0 ;
        alias.window_msg_initMember.call(this);
    };
    alias.window_msg_newPage = Window_Message.prototype.newPage
    Window_Message.prototype.newPage = function(textState) {
        alias.window_msg_newPage.call(this, textState);
        this._autoWait = textState.text.length * autoWait * frame;    

    };    
    /**
     * @override
     */
    Window_Message.prototype.updateInput = function() {
        if (this.isAnySubWindowActive()) {
            return true;
        }
        if (this.pause && !_active) {
            if (this.isTriggered()) {
                this.updateNextPage();
            }
            return true;
        }
        else if (this.pause && _active) {
            /** 點擊仍然讓他往下一句 */
            if (this.isTriggered()) {
                this.updateNextPage();
                return true;
            }
            /** 沒有點擊就等待 */
            if (this._autoWait > 0) {                
                this._autoWait--;
                return true;
            }
            if (this._autoWait <= 0) {
                this._autoWait = 0;
            }
            this.updateNextPage();
            return true;
        }
        return false;
    };    
    Window_Message.prototype.updateNextPage = function (){
        Input.update();
        this.pause = false;
        if (!this._textState) {
            this.terminateMessage();
        }        
    }

})();