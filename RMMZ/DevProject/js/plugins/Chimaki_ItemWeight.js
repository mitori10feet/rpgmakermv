//=============================================================================
// RPG Maker MZ Chimaki_ItemWeight
// Version: 1.0
//=============================================================================
/*:
 * @target MZ
 * @plugindesc add item weight
 * @author Chimaki
 * 
 * 
 * @param mode
 * @desc 1:cant take item when over weight, 2: can take item when over weight
 * @type number
 * @default 1
 * 
 * @param overEvent
 * @desc call commandEvent when you add item
 * @type number
 * @default 1
 * 
 * 
 * @param punishment
 * @desc (mode=2)，call commandEvent after you over weight (ex: speed down )
 * @type number
 * @default 2
 * 
 * @param remove_punishment
 * @desc (mode=2) call commandEvent after yoy not over weight(es: reset speed)
 * @type number
 * @default 3
 * 
 * @param maxWeight
 * @desc set max weight
 * @type number
 * @default 1000
 *
 * @param textWeight
 * @desc weight on ui, if you want use icon, do not set this param
 * @type string
 * @default
 *   
 * @param iconWeight
 * @desc weight icon id 
 * @type number
 * @default 209
 * 
 * 
 * @command addWeight
 * @text add max weight
 * @desc add max weight
 * 
 * @arg add
 * @type number
 * 
 * 
 * @help Chimaki_ItemWeight.js
 * set the weight item/ weapon / armor at note column 
 * EX: <weight:0> default = 0 if you not set any thing
 * 
 * you can set the mode with 1, than you cant take new item if it will over weight, and set message with commandEvent 
 * "overEvent"
 * 
 * if you want to can take item when over weight, plz set the mode = 2 
 * then punishment event will call when you over weight
 * remove_punishment will call when you not over weight
 * 
 * 
*/


/*:zh_TW
 * @target MZ
 * @plugindesc add item weight
 * @author Chimamki
 * 
 * 
 * @param mode
 * @desc 1: 過重不可拿取道具，並觸發公用事件overEvent, 2:過中仍可拿道具，並觸發公用事件punishment
 * 
 * @type number
 * @default 1
 * 
 * @param overEvent
 * @desc 過重時呼叫事件，預設為公用事件1
 * @type number
 * @default 1
 * 
 * 
 * @param punishment
 * @desc 過重時的懲罰事件，預設為公用事件2(你可以在該事件中設置移動速度變慢...etc)
 * @type number
 * @default 2
 * 
 * @param remove_punishment
 * @desc 配合punishment使用，非過重狀態時恢復原本狀態的公用事件ID( EX: 恢復移動速度)
 * @type number
 * @default 3
 * 
 * @param maxWeight
 * @desc set max weight in party
 * @type number
 * @default 1000
 *
 * @param textWeight
 * @desc 負重文字，想使用icon時，請勿設置此參數(textWeight, iconWeight只能擇一使用)
 * @type string
 * @default
 *   
 * @param iconWeight
 * @desc 負重icon 預設209，如果要設置icon請不要設置 iconText參數
 * @type number
 * @default 209
 * 
 * 
 * @command addWeight
 * @text 增加最大負重
 * @desc 增加最大負重，可輸入負數
 * 
 * @arg add
 * @type number
 * 
 * 
 * @help Chimaki_ItemWeight.js
 * 提供基本重量設定，使用事件中的新增武器/道具/防具時將自動判斷
 * 若新增道具時超過重量會呼叫 overEvent 中設定的一般事件，且新增道具會失敗
 * 在資料庫備註欄加上 <weight:這邊寫重量的數字> 即可增加道具重量，沒有設定的
 * 道具重量則為0
 * 
*/


var alias = alias || {};
(() => {
    'use strict'
    const pluginName = 'Chimaki_ItemWeight';
    /** 過重顯示 */
    const maxWeight = Number(PluginManager.parameters(pluginName)['maxWeight']);
    const iconWeight = Number(PluginManager.parameters(pluginName)['iconWeight']);
    const textWeight = String(PluginManager.parameters(pluginName)['textWeight']);
    const MODE = { CANT_GET : 1, CAN_GET : 2 };

    /** 過重效果 */
    const mode = Number(PluginManager.parameters(pluginName)['mode']);
    const overEvent = Number(PluginManager.parameters(pluginName)['overEvent'])
    const punishment = Number(PluginManager.parameters(pluginName)['punishment']);
    const remove_punishment = Number(PluginManager.parameters(pluginName)['remove_punishment']);



    PluginManager.registerCommand(pluginName, "addWeight", args => {
        $gameParty.addMaxWeight(Number(args.add));
    });
    /**
     * @override
     */
    Game_Party.prototype.gainItem = function(item, amount, includeEquip) {
        /** 新增之前判斷 */

        /** 模式1直接return 觸發事件 */
        if (mode == MODE.CANT_GET && this.isOverWeight(item, amount)) {
            $gameTemp.reserveCommonEvent(overEvent);
            return;
        }
        const container = this.itemContainer(item);
        if (container) {
            const lastNumber = this.numItems(item);
            const newNumber = lastNumber + amount;
            container[item.id] = newNumber.clamp(0, this.maxItems(item));
            if (container[item.id] === 0) {
                delete container[item.id];
            }
            if (includeEquip && newNumber < 0) {
                this.discardMembersEquip(item, -newNumber);
            }
            this.refreshWeight();
            $gameMap.requestRefresh();
        }
    };
    Game_Party.prototype.isOverWeight = function (item, number){
        const weight = Number(item.meta.weight || 0);
        return this._currentWeight + (weight * number) > maxWeight;
    }
    Game_Party.prototype.currentWeight = function () {
        return this._currentWeight;
    }
    Game_Party.prototype.maxWeight = function () {
        return this._maxWeight;
    }    
    Game_Party.prototype.addMaxWeight = function (value) {
        this._maxWeight += value;
        this.refreshWeightState();
        
    }        

    /**
     * @override
     */
    Game_Party.prototype.initialize = function() {
        Game_Unit.prototype.initialize.call(this);
        this._gold = 0;
        this._steps = 0;
        this._lastItem = new Game_Item();
        this._menuActorId = 0;
        this._targetActorId = 0;
        this._actors = [];
        this._maxWeight = maxWeight;
        this._currentWeight = 0;
        this._lastWeightState = false;
        this.initAllItems();
    };
    /**
     * @description 刷新身上總重量，懲罰模式則觸發事件或是恢復事件
     */
    Game_Party.prototype.refreshWeight = function () {
        this._currentWeight = 0;
        for (let key in this._items){
            const item = $dataItems[key];
            const num = this._items[key] ;
            const weight = Number(item.meta.weight || 0);
            this._currentWeight += weight * num;
        }
        for (let key in this._weapons){
            const item = $dataWeapons[key];
            const num = this._weapons[key] ;
            const weight = Number(item.meta.weight || 0);
            this._currentWeight += weight * num;
        }        
        for (let key in this._armors){
            const item = $dataArmors[key];
            const num =  this._armors[key];
            const weight = Number(item.meta.weight || 0);
            this._currentWeight += weight * num;
        }     
        this.refreshWeightState();

    }
    Game_Party.prototype.refreshWeightState = function () {
        const state = this.nowOverWeight();
        if (state != this._lastWeightState) {
            if (MODE.CAN_GET == mode ) {
                if (this.nowOverWeight()) {
                    $gameTemp.reserveCommonEvent(punishment)
                }
                else {
                    $gameTemp.reserveCommonEvent(remove_punishment);
                }
            }

            this._lastWeightState = state;
        }
    }
    Game_Party.prototype.checkOverWeight =  function (value) {
        return value > this._maxWeight
    }

    Game_Party.prototype.nowOverWeight =  function () {
        return this._currentWeight > this._maxWeight
    }

    Scene_Menu.prototype.create = function() {
        Scene_MenuBase.prototype.create.call(this);
        this.createCommandWindow();
        this.createGoldWindow();
        this.createWeightWindow();
        this.createStatusWindow();
    };
    Scene_Menu.prototype.createWeightWindow = function () {
        const rect = this.weightWindowHeight();
        this._weightWindow = new Window_Weight(rect) ;
        this.addWindow(this._weightWindow);
    }
    Scene_Menu.prototype.commandWindowRect = function() {
        const ww = this.mainCommandWidth();
        const wh = this.mainAreaHeight() - this.goldWindowRect().height - this.weightWindowHeight().height;
        const wx = this.isRightInputMode() ? Graphics.boxWidth - ww : 0;
        const wy = this.mainAreaTop();
        return new Rectangle(wx, wy, ww, wh);
    }

    Scene_Menu.prototype.weightWindowHeight = function () {
        const ww = this.mainCommandWidth();
        const wh = this.calcWindowHeight(1, true);
        const wx = this.isRightInputMode() ? Graphics.boxWidth - ww : 0;
        const wy = this.mainAreaBottom() - wh - this.goldWindowRect().height;
        return new Rectangle(wx, wy, ww, wh);        
    }

    /**
     * @override
     * @description 商店處理
     */
     alias.scene_shop_create = Scene_Shop.prototype.create;
     Scene_Shop.prototype.create = function() {
        alias.scene_shop_create.call(this);
        this.crateWeightWindow();
    }
    Scene_Shop.prototype.crateWeightWindow = function () {
        this._weightWindow = new Window_Weight(this.weightWindowRect());
        this._weightWindow.hide();
        this._numberWindow.setWeightWindow(this._weightWindow);
        this.addWindow(this._weightWindow);
    }
    Scene_Shop.prototype.statusWindowRect = function() {
        const ww = this.statusWidth();
        const wh = this._dummyWindow.height - this._goldWindow.height;
        const wx = Graphics.boxWidth - ww;
        const wy = this._dummyWindow.y;
        return new Rectangle(wx, wy, ww, wh);
    };    
    Scene_Shop.prototype.weightWindowRect = function() {
        const ww = this.statusWidth();
        const wh = this._goldWindow.height;
        const wx = Graphics.boxWidth - ww;
        const wy = this.statusWindowRect().y + this.statusWindowRect().height;
        return new Rectangle(wx, wy, ww, wh);
    };
    alias.scene_shop_activateBuyWindow = Scene_Shop.prototype.activateBuyWindow
    Scene_Shop.prototype.activateBuyWindow = function() {
        alias.scene_shop_activateBuyWindow.call(this);
        this._weightWindow.show();
    };    
    alias.scene_shop_activateSellWindow = Scene_Shop.prototype.activateSellWindow;
    Scene_Shop.prototype.activateSellWindow = function() {
        alias.scene_shop_activateSellWindow.call(this);
        this._weightWindow.hide();
    };    
    alias.scene_shop_onBuyCancel = Scene_Shop.prototype.onBuyCancel;
    Scene_Shop.prototype.onBuyCancel = function() {
        alias.scene_shop_onBuyCancel.call(this);
        this._weightWindow.hide();
    };
    /**
     * @override 
     * @description 擴充mode = 1 且超過上限不可購買
     */
    Scene_Shop.prototype.onNumberOk = function() {
        if (MODE.CANT_GET == mode && $gameParty.checkOverWeight(this._numberWindow.tempWeight()) ) {
            SoundManager.playBuzzer(); 
            this._numberWindow.activate();
            return;
        }
        SoundManager.playShop();
        switch (this._commandWindow.currentSymbol()) {
            case "buy":
                this.doBuy(this._numberWindow.number());
                break;
            case "sell":
                this.doSell(this._numberWindow.number());
                break;
        }
        this.endNumberInput();
        this._goldWindow.refresh();
        this._statusWindow.refresh();
    }; 
    alias.windowShopNumber = Window_ShopNumber.prototype.initialize;
    Window_ShopNumber.prototype.initialize = function(rect) {
        alias.windowShopNumber.call(this, rect);
        this._tempWeight = 0;

    };       
    Window_ShopNumber.prototype.setWeightWindow = function( window ) {
        this._weightWindow = window;
    };    
    Window_ShopNumber.prototype.refreshWeightWindow = function() {
        if (!this._weightWindow) return;
        this._tempWeight = 0;
        const current = $gameParty.currentWeight();
        const item = SceneManager._scene._item;
        const weight = (Number(item.meta.weight) || 0 ) * this._number;
        this._tempWeight = current + weight;
        this._weightWindow.refresh(current + weight);
    };    
    Window_ShopNumber.prototype.tempWeight = function () {
        return this._tempWeight;
    }

    alias.window_shopNumber_refresh = Window_ShopNumber.prototype.refresh;
    Window_ShopNumber.prototype.refresh = function() {
        alias.window_shopNumber_refresh.call(this);
        this.refreshWeightWindow();
    };    

    class Window_Weight extends Window_Gold {
        constructor (rect) {
            super(rect);
            
        }
        colSpacing () {
            return 0;
        };

        refresh ( value1 ) {
            const rect = this.itemLineRect(0);
            const x = rect.x;
            const y = rect.y;
            const width = rect.width;
            this.contents.clear();
            
            if (MODE.CAN_GET == mode) {
                this.drawCurrencyValuePlus(`${value1 ? value1 : this.value()}`, ` / ${$gameParty.maxWeight()}`, textWeight, x, y, width)
            }
            else {
                this.drawCurrencyValue(`${value1 ? value1 : this.value()} / ${$gameParty.maxWeight()}`, textWeight, x, y, width);
            }

        };
        drawCurrencyValuePlus (value, value2, unit, x, y, width) {
            const unitWidth = unit ? Math.min(80, this.textWidth(unit)) : 24;
            const allWidth = this.contents.width - unitWidth;
            const rightWidth = this.textWidth(value2);

            this.resetTextColor();
            if ($gameParty.nowOverWeight()) {
                this.changeTextColor(ColorManager.textColor(11));    
            }
            this.drawText(value, x , y, allWidth / 2 - 20 , "right");        
            this.resetTextColor();
            this.drawText(value2, allWidth / 2 - 10 , y, allWidth / 2,'left')
            this.changeTextColor(ColorManager.systemColor());            
            unit ? this.drawText(unit, x + width - unitWidth + 6, y, unitWidth, "right") :
            this.drawIcon(iconWeight , width - unitWidth + 6, y);        
        }
        drawCurrencyValue (value, unit, x, y, width) {

            const unitWidth = unit ? Math.min(80, this.textWidth(unit)) : 24;
            this.resetTextColor();
            this.drawText(value, x, y, width - unitWidth - 6, "right");
            this.changeTextColor(ColorManager.systemColor());
            unit ? this.drawText(unit, x + width - unitWidth, y, unitWidth, "right") :
            this.drawIcon(iconWeight , width - unitWidth + 5 , y);
        }
        value () {
            return $gameParty.currentWeight();
        };
        currencyUnit () {
            return textWeight;
        };
        open () {
            this.refresh();
            Window_Selectable.prototype.open.call(this);
        };        
    }

})()

