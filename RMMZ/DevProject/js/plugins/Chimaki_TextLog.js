//=============================================================================
// RPG Maker MZ Chimaki_TextLog
// Version: 1.0
//=============================================================================
/*:
 * @target MZ
 * @plugindesc key donw "P" , get screenshot
 * @author Chimaki
 * 
 * @param saveVar
 * @desc 用來儲存對話文本的變數id
 * @type number
 * @default 4
 * 
*/


var alias = alias || {};
alias.scene = alias.scene || {}
const pluginName = "Chimaki_TextLog";
( () => {

    alias.scene.sceneBootStart = Scene_Boot.prototype.start
    Scene_Boot.prototype.start = function () {
        alias.scene.sceneBootStart.call(this);
        SceneManager.showDevTools();
    }

    alias.scene.sceneMapCreateAllWindows = Scene_Map.prototype.createAllWindows;
    Scene_Map.prototype.createAllWindows = function () {
        alias.scene.sceneMapCreateAllWindows.call(this);
        this.createLogWindow()

    }
    // Window_Base.prototype.drawText = function(text, x, y, maxWidth, align) {
    Scene_Map.prototype.createLogWindow = function () {
        const rect = this.logWindowRect()
        this._logWindow = new WindowLog(rect);
        this._logWindow.drawText(`LOG`, -7, -12, 80, 'center')
        this.addWindow(this._logWindow);
    }
    Scene_Map.prototype.logWindowRect = function () {
        const ww = 80;
        const wh = 36;
        const wx = Graphics.width - ww - 3;
        const wy = Graphics.height - wh - 3;
        return new Rectangle(wx, wy, ww, wh);        

    }

    
    const variable = Number(PluginManager.parameters(pluginName)['saveVar']);
    /**
     * @description 只針對人物對話進行紀錄，儲存到變數中
     * @override 
     * @param {*} params 
     */
    Game_Interpreter.prototype.command101 = function(params) {
        if ($gameMessage.isBusy()) {
            return false;
        }
        $gameMessage.setFaceImage(params[0], params[1]);
        $gameMessage.setBackground(params[2]);
        $gameMessage.setPositionType(params[3]);
        $gameMessage.setSpeakerName(params[4]);

        let content = $gameVariables.value(variable);
        console.log(`content `, content);
        let name = params[4];
        let nameFalg = false;

        while (this.nextEventCode() === 401) {
            // Text data
            this._index++;
            const param = this.currentCommand().parameters[0];
            console.log(`chimaki param`, param, params[4]);
            
            if (name && !nameFalg) {
                content += `\n${name} :\n ${param}`;
                nameFalg = true;
            }
            else {
                content += '\n' + param;
            }

            $gameMessage.add(param);
        }
        switch (this.nextEventCode()) {
            case 102: // Show Choices
                this._index++;
                this.setupChoices(this.currentCommand().parameters);
                break;
            case 103: // Input Number
                this._index++;
                this.setupNumInput(this.currentCommand().parameters);
                break;
            case 104: // Select Item
                this._index++;
                this.setupItemChoice(this.currentCommand().parameters);
                break;
        }
        this.setWaitMode("message");
        $gameVariables.setValue(variable, content)
        return true;
    };

    class WindowLog extends Window_Base {
        constructor (...args){
            super(...args)
        }
        initialize (...args) {
            Window_Base.prototype.initialize.call(this, ...args);
            this.resetFontSettings();

        }
        resetFontSettings () {
            this.contents.fontFace = $gameSystem.mainFontFace();
            this.contents.fontSize = 13;
            this.resetTextColor();
        };  
        update () {
            Window_Base.prototype.update.call(this);
            this.processTouch();

        };              
        processTouch () {
            if (TouchInput.isClicked()){
                if (this.isTouchedInsideFrame()){
                    console.error(`chimaki touch !`)
                }
            }
        }
        isTouchedInsideFrame () {
            const touchPos = new Point(TouchInput.x, TouchInput.y);
            const localPos = this.worldTransform.applyInverse(touchPos);
            return this.innerRect.contains(localPos.x, localPos.y);
        };        
    }
})()