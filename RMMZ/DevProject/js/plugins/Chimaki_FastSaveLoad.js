//=============================================================================
// RPG Maker MZ Chimaki_FastSaveLoad
// Version: 1.0
//=============================================================================
/*:
 * @target MZ
 * @plugindesc 
 * @author Chimaki
 * 
 * @param fastSave
 * @desc 設定快速存擋的熱鍵號碼，請用https://keycode.info/ 查詢對應號碼 (預設為鍵盤S)
 * @type number
 * @default 83
 * 
 * @param saveMsg
 * @desc 快速存擋後呼叫公用事件的事件id
 * @type number
 * @default 1
 * 
 * @param saveAutoSave
 * @desc 設定為true: 讀取時，只儲存快速存檔
 * @type boolean
 * @default false
 * 
 *  
 * @param loadAutoSave
 * @desc 設定為true: 讀取時，只讀取快速存檔
 * @type boolean
 * @default false
 * 
 * 
 * @param fastLoad
 * @desc 設定快速讀擋的熱鍵號碼，請用https://keycode.info/ 查詢對應號碼 (預設為鍵盤D)
 * @type number
 * @default 76
 * 
 * @command switchAutoSave
 * @text 切換是否開啟此功能
 * 
 * @arg switch
 * @type boolean
 * @default true
 * 
 * @help
 * 請使用這個網站查詢 keycode https://keycode.info/ 去修改熱鍵值
 * 存檔只提供在地圖畫面上時使用
 * 
*/



/**
 * 用熱鍵號碼決定快速存擋/讀取檔案，使用key code 來執行
 */

var alias = alias || {};
alias.fastSave = alias.fastSave || {};
(() => {
    var lastSave = 1;
    var lastLoad = 1;
    'use strict'
    
    const pluginName = 'Chimaki_FastSaveLoad';
    
    const fastSave = Number(PluginManager.parameters(pluginName)['fastSave']);
    const fastLoad = Number(PluginManager.parameters(pluginName)['fastLoad']);
    const saveMsg =  Number(PluginManager.parameters(pluginName)['saveMsg']);
    const saveAutoSave = Boolean(PluginManager.parameters(pluginName)['saveAutoSave']);
    const loadAutoSave = Boolean(PluginManager.parameters(pluginName)['loadAutoSave']);

    let _active = true;
    PluginManager.registerCommand(pluginName, "switchAutoSave", args => {
        _active = args.switch == "true" ? true : false;
    });

    alias.fastSave = SceneManager.onKeyDown;
    SceneManager.onKeyDown = function(event) {
        alias.fastSave.call(this, event);
        let index = DataManager.latestSavefileId();
        if (!event.ctrlKey && !event.altKey) {
            /** 關閉則不做後續動作 */
            if (!_active) return;

            if (event.keyCode === fastSave) {   
                if ( SceneManager._scene instanceof Scene_Map){
                    if (saveAutoSave) index = 0;
                    this.executeSave(index);
                }
                
            } 
            if (event.keyCode === fastLoad) {
                if (loadAutoSave) index = 0;
                this.executeLoad(index);                
            } 
        }
    }
    SceneManager.executeSave = function(savefileId) {
        $gameSystem.setSavefileId(savefileId);
        $gameSystem.onBeforeSave();        
        DataManager.saveGame(savefileId)
            .then(() => this.onSaveSuccess())
            .catch(() => this.onSaveFailure());
    };
    SceneManager.onSaveSuccess = function() {
        SoundManager.playSave();   
        $gameTemp.reserveCommonEvent(saveMsg);
        
    };   
    SceneManager.onSaveFailure = function() {
        SoundManager.playBuzzer();
    };     

    SceneManager.executeLoad = function(savefileId) {
        const scene = SceneManager._scene;
        
        DataManager.loadGame(savefileId)
            .then(() => this.onLoadSuccess())
            .catch(() => this.onSaveFailure());
    }; 
    SceneManager.fadeSpeed = function() {
        return 24;
    };     
    SceneManager.onLoadSuccess = function () {
        const scene = SceneManager._scene;
        SoundManager.playLoad();
        scene.fadeOutAll();
        this.reloadMapIfUpdated();
        SceneManager.goto(Scene_Map);
        this._loadSuccess = true;
        const nowCcene = SceneManager._scene;

    }
    SceneManager.reloadMapIfUpdated = function() {
        if ($gameSystem.versionId() !== $dataSystem.versionId) {
            const mapId = $gameMap.mapId();
            const x = $gamePlayer.x;
            const y = $gamePlayer.y;
            $gamePlayer.reserveTransfer(mapId, x, y);
            $gamePlayer.requestMapReload();
        }
    };    
 


    
    Scene_Save.prototype.onSavefileOk = function() {
        Scene_File.prototype.onSavefileOk.call(this);
        const savefileId = this.savefileId();
        if (this.isSavefileEnabled(savefileId)) {
            this.executeSave(savefileId);
            lastSave = savefileId;
        } else {
            this.onSaveFailure();
        }
    };
    
    Scene_Load.prototype.onSavefileOk = function() {
        Scene_File.prototype.onSavefileOk.call(this);
        const savefileId = this.savefileId();
        if (this.isSavefileEnabled(savefileId)) {
            this.executeLoad(savefileId);
            lastLoad = savefileId;
        } else {
            this.onLoadFailure();
        }
    };
})()