//=============================================================================
// Chimaki_8dir.js
// Version: 1.0
//=============================================================================
/*:
 * @target MZ
 * @plugindesc ８方向移動
 * @author Chimaki
 * 
 * @help Chimaki_8dir.js
 * RMMZ 8 方向移動plugins ， dirSwitch 決定是否開啟此功能
 * 
 * 
 * @param dirSwitch
 * @type boolean
 * @default false
 * 
 * 
 */


(() => {
    'use strict'
    const pluginName = "Chimaki_8dir";
    let chimaki_params = PluginManager.parameters(pluginName);
    let dir_switch = Boolean(chimaki_params['dirSwitch'] || false);

    Game_Player.prototype.getInputDirection = function() {
        return dir_switch ?  Input.dir8 : Input.dir4;
    };
    /**
     * 改變地圖移動
     */
    Game_Map.prototype.xWithDirection = function(x, d) {
        return x + (d === 6 || d === 3 || d === 9 ? 1 : d === 1 || d === 7 || d === 4 ? -1 : 0);
    };
    
    Game_Map.prototype.yWithDirection = function(y, d) {
        return y + (d === 2 || d === 1 || d === 3 ? 1 : d === 8 || d === 7 || d === 9 ? -1 : 0);
    };
    
    Game_Map.prototype.roundXWithDirection = function(x, d) {        
        return this.roundX(x + (d === 6 || d === 3 || d == 9 ? 1 : d === 4 || d === 1 || d === 7 ? -1 : 0) );
    };
    
    Game_Map.prototype.roundYWithDirection = function(y, d) {
        return this.roundY(y + (d === 2 || d === 3 || d === 1 ? 1  : d === 8 || d === 7 || d === 9 ? -1 : 0));
    };

    // 讓移動時候圖片正常
    Sprite_Character.prototype.characterPatternY = function() {
        let dir = this._character.direction();

        if (dir == 2 || dir  == 1 || dir == 3){
            dir = 0
        }
        else if (dir == 4 ){
            dir = 1;
        }
        else if (dir == 6){
            dir = 2;
        }
        else if (dir == 8 || dir == 7 || dir == 9){
            dir = 3;
        }

        return dir;
    };
})();
